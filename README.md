**Payment gateway**

Sandbox payment gateway with SDK.

**How to run**

**Payment gateway**

Open *PaymentGateway.sln* using Visual Studio 2017 or newer

Run *PaymentGateway.API* using F5 and wait till Swagger console is opened.

Use swagger console or your favourite app (i.e. Postman) to interact with API (*default: https://localhost:44355/v1/payments*)

Application metrics are collected by *App Metrics* (https://www.app-metrics.io/) and available on */metrics* or */metrics-text* 

**SDK**

Open *PaymentGateway.Client.sln* in another Visual Studio window (2017 or newer)

Run *PaymentGateway.IntegrationSample* using F5 while *PaymentGateway.API* is running


**Comments and suggestions**

* Read payments one-by-one for reconciliation doesn't seem the most effective method. Batching or streaming are more effective approaches for such purposes.
* Usage of full card credentials for every payment is not secure and excessive. If it is possible, prefer using payment method token after the first successful payment 
* If interaction with bank providers has fees, naive card validation should be moved to gateway code to reduce potential costs.
* Metric reporting can be targeted to another output including Graphana, Graphite, etc. for further use.