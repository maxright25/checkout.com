﻿using PaymentGateway.Payments;
using System;

namespace PaymentGateway
{
    public interface IPaymentGateway : IDisposable
    {
        IPaymentsClient Payments { get; }
        
        //easily expandable with another parts of gateway
        //
        //ICustomerClient Customers { get; }
        //
        //IReconciliationClient Reconciliations { get; }
        //
    }
}
