﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Client.Extensions
{
    public static class HttpResponseMessageExtensions
    {
        public static Task<T> ReadAsync<T>(this HttpResponseMessage response)
        {
            return response.EnsureSuccessStatusCode()
                    .Content
                    .ReadAsAsync<T>();
        }
    }
}
