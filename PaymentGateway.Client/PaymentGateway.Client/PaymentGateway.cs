﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using PaymentGateway.Payments;

namespace PaymentGateway
{
    public class PaymentGateway : IPaymentGateway
    {
        private static readonly Regex VersionRegex = new Regex("/v\\d/?", RegexOptions.Compiled);
        private static readonly TimeSpan DefaultTimeout = TimeSpan.FromMinutes(10);

        public IPaymentsClient Payments { get; }

        public PaymentGateway(string endpointUri)
        {
            if (string.IsNullOrWhiteSpace(endpointUri))
                throw new ArgumentNullException(nameof(endpointUri));

            //authentication/authorization handler can be easily added to the pipeline here -->
            var httpClient = HttpClientFactory.Create();

            var uri = GetUri(endpointUri);

            httpClient.Timeout = DefaultTimeout;
            httpClient.BaseAddress = uri;

            Payments = new PaymentsClient(httpClient);
        }

        public void Dispose()
        {
            Payments?.Dispose();
        }

        private static Uri GetUri(string endpointAddress)
        {
            if (VersionRegex.IsMatch(endpointAddress))
                throw new ArgumentException("Payment gateway uri must not include api versions");

            var normalizedEndpoint = endpointAddress.EndsWith("/") ? endpointAddress : $"{endpointAddress}/";

            if (!Uri.TryCreate(normalizedEndpoint, UriKind.Absolute, out var uri))
            {
                throw new ArgumentException("Payment gateway uri is not well-formed");
            }

            return uri;
        }
    }
}
