﻿using Newtonsoft.Json;
using PaymentGateway.Client.Extensions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PaymentGateway.Client
{
    public interface IEndpoint : IDisposable
    {
    }

    internal abstract class EndpointBase : IEndpoint
    {
        private readonly HttpClient _client;

        protected abstract int Version { get; }

        protected EndpointBase(HttpClient client)
        {
            _client = client;
        }

        public void Dispose()
        {
            _client?.Dispose();
        }

        protected internal async Task<T> GetAndEnsureAsync<T>(string url, bool returnNullOn404 = false) where T : class
        {
            using (var response = await SendAsync(HttpMethod.Get, url).ConfigureAwait(false))
            {
                if (response.StatusCode == HttpStatusCode.NotFound && returnNullOn404)
                    return null;

                response.EnsureSuccessStatusCode();

                return await response.ReadAsync<T>().ConfigureAwait(false);
            }
        }

        protected internal async Task PostAndEnsureAsync<T>(string url, T content)
        {
            using (var response = await PostAsync(url, content).ConfigureAwait(false))
            {
                response.EnsureSuccessStatusCode();
            }
        }

        protected internal async Task<TOut> PostAndEnsureAsync<TIn, TOut>(string url, TIn content)
        {
            using (var response = await PostAsync(url, content).ConfigureAwait(false))
            {
                return await response.ReadAsync<TOut>().ConfigureAwait(false);
            }
        }

        protected internal Task<HttpResponseMessage> PostAsync<TIn>(string url, TIn content)
        {
            return SendAsync(HttpMethod.Post, url, content);
        }

        private Task<HttpResponseMessage> SendAsync<TIn>(HttpMethod method, string url, TIn content)
        {
            var message = new HttpRequestMessage(method, ConstructUrl(url))
            {
                Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json")
            };

            return _client.SendAsync(message, Token);
        }

        private Task<HttpResponseMessage> SendAsync(HttpMethod method, string url)
        {
            var message = new HttpRequestMessage(method, ConstructUrl(url));

            return _client.SendAsync(message, Token);
        }

        private string ConstructUrl(string actionUrl)
        {
            return $"v{Version}/{actionUrl}";
        }

        private static CancellationToken Token => CancellationToken.None;
    }
}
