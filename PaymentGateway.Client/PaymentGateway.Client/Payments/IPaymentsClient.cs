﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Payments
{
    public interface IPaymentsClient : IDisposable
    {
        /// <summary>
        /// Create the payment
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<Payment> CreateAsync(PaymentRequest request);
            
        /// <summary>
        /// Retrieve the existing payment by payment identifier
        /// </summary>
        /// <param name="paymentId">Payment identifier</param>
        /// <returns></returns>
        Task<Payment> GetAsync(string paymentId);
    }
}
