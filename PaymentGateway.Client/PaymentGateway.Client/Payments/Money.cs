﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Payments
{
    public struct Money
    {
        public int MinorUnits { get; set; }

        public decimal Value => (decimal)MinorUnits / 100;

        public string Currency { get; set; }

        public static Money Zero => new Money(0, null);

        public Money(int minorUnits, string currency)
        {
            MinorUnits = minorUnits;
            Currency = currency;
        }

        public Money(decimal amount, string currency)
        {
            MinorUnits = (int)(amount * 100);
            Currency = currency;
        }


        private bool Equals(Money other)
        {
            return (MinorUnits == 0 && other.MinorUnits == 0) ||
                   (MinorUnits == other.MinorUnits &&
                    string.Equals(Currency, other.Currency, StringComparison.InvariantCultureIgnoreCase));
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Money)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (MinorUnits * 397) ^ (Currency != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(Currency) : 0);
            }
        }

        public static bool operator ==(Money left, Money right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Money left, Money right)
        {
            return !Equals(left, right);
        }

        public static Money operator +(Money left, Money right)
        {
            if (left != Zero && right != Zero && left.Currency != right.Currency)
                throw new InvalidOperationException();

            return new Money(left.MinorUnits + right.MinorUnits, left.Currency ?? right.Currency);
        }

        public static Money operator -(Money left, Money right)
        {
            if (left != Zero && right != Zero && left.Currency != right.Currency)
                throw new InvalidOperationException();

            return new Money(left.MinorUnits - right.MinorUnits, left.Currency ?? right.Currency);
        }

        public override string ToString()
        {
            return $"{Value:F2} {Currency}";
        }
    }
}
