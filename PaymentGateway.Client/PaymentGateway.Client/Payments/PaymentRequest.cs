﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Payments
{
    public class PaymentRequest
    {
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CardholderName { get; set; }
        public string CVV { get; set; }
        public string PaymentMethodToken { get; set; }
        public Money Amount { get; set; }
    }
}
