﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Payments
{
    public class PaymentMethod
    {
        public string Id { get; set; }
        public string MaskedNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CardholderName { get; set; }
    }
}
