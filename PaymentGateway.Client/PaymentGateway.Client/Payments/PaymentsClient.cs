﻿using PaymentGateway.Client;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Payments
{
    internal class PaymentsClient : EndpointBase, IPaymentsClient
    {
        protected override int Version => 1;

        public PaymentsClient(HttpClient client) : base(client)
        {

        }

        public Task<Payment> CreateAsync(PaymentRequest request)
        {
            var url = "payments";
            return PostAndEnsureAsync<PaymentRequest, Payment>(url, request);
        }

        public Task<Payment> GetAsync(string paymentId)
        {
            var url = $"payments/{paymentId}";
            return GetAndEnsureAsync<Payment>(url, true);
        }
    }
}
