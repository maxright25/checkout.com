﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Payments
{
    public class Payment
    {
        public string Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public Money Amount { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public bool IsSuccess { get; set; }
        public string ResponseText { get; set; }
        public int ResponseCode { get; set; }
    }
}
