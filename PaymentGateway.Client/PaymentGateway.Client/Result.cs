﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway
{
    public class Result
    {
        public bool IsSuccess { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
    }

    public class Result<T> : Result
    {
        public T Target { get; set; }
    }
}
