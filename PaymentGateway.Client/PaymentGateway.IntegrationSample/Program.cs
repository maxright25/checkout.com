﻿using PaymentGateway.Payments;
using System;
using System.Threading.Tasks;

namespace PaymentGateway.IntegrationSample
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await PaymentAttemptAsync().ConfigureAwait(false);
            Console.ReadLine();
        }

        private static async Task PaymentAttemptAsync()
        {
            try
            {
                Console.WriteLine("Payment attempt...");

                var gateway = new PaymentGateway("https://localhost:44355");

                var paymentSample = new PaymentRequest()
                {
                    Amount = new Money(1000, "USD"),
                    CardholderName = "John Doe",
                    CardNumber = "5444444444444444",
                    CVV = "012",
                    ExpiryDate = "20/20",
                };

                var paymentResponse = await gateway.Payments.CreateAsync(paymentSample).ConfigureAwait(false);

                Console.WriteLine("Payment attempt completed");
                Console.WriteLine($"--> Payment {paymentResponse.Id}");
                Console.WriteLine($"--> IsSuccess {paymentResponse.IsSuccess}");
                Console.WriteLine($"--> Payment Method Token {paymentResponse.PaymentMethod?.Id}");

                Console.WriteLine("Trying to retrieve the existing payment");

                var payment2 = await gateway.Payments.GetAsync(paymentResponse.Id).ConfigureAwait(false);

                Console.WriteLine("Retrieve attempt completed");

                Console.WriteLine($"--> Payment {payment2.Id}");
                Console.WriteLine($"--> IsSuccess {payment2.IsSuccess}");
                Console.WriteLine($"--> Payment Method Token {payment2.PaymentMethod?.Id}");

            } catch (Exception ex)
            {
                Console.WriteLine("Something really bad happened");
                Console.WriteLine(ex);
            }
        }
    }
}
