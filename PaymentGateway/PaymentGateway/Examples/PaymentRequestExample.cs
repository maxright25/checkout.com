﻿using PaymentGateway.Entities;
using Swashbuckle.AspNetCore.Examples;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentGateway.API.Examples
{
    public class PaymentRequestExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new PaymentRequest()
            {
                Amount = new Money(1000, "USD"),
                CardholderName = "John Doe",
                CardNumber = "5444444444444444",
                CVV = "012",
                ExpiryDate = "20/20",
            };
        }
    }
}
