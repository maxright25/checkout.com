﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Mvc;
using PaymentGateway.API.Examples;
using PaymentGateway.Business;
using PaymentGateway.Entities;
using Swashbuckle.AspNetCore.Examples;
using Payment = PaymentGateway.Entities.Payment;

namespace PaymentGateway.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly IPaymentsService _service;

        public PaymentsController(IPaymentsService service)
        {
            _service = service;
        }

        // GET api/payments/id
        [HttpGet("{paymentId}")]
        public async Task<ActionResult<Payment>> GetAsync(string paymentId)
        {
            var transaction = await _service.GetAsync(paymentId).ConfigureAwait(false);
            if (transaction == null)
                return NotFound();

            return transaction;
        }

        // POST api/payments
        [HttpPost]
        [SwaggerRequestExample(typeof(PaymentRequest), typeof(PaymentRequestExample))]
        public Task<Payment> CreateAsync([FromBody] PaymentRequest request)
        {
            return _service.CreateAsync(request);
        }
    }
}
