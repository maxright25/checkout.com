﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Business
{
    public class AutofacBusinessModule : Module
    {
        public AutofacBusinessModule()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BankProviderFactory>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterType<SandboxBankClient>()
                .Named<IBankProvider>("sandbox")
                .SingleInstance();

            builder.RegisterType<PaymentService>()
                .AsImplementedInterfaces()
                .SingleInstance();
        
            builder.RegisterType<PaymentMethodsService>()
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}
