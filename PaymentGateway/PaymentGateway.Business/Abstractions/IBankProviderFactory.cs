﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Business
{
    public interface IBankProviderFactory
    {
        IBankProvider GetProvider(string cardNumber);
        IBankProvider GetProviderByBankName(string bankName);
    }
}
