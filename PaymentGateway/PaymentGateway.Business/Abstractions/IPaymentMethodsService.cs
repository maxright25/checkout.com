﻿using PaymentGateway.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Business
{
    public interface IPaymentMethodsService
    {
        Task<PaymentMethod> CreatePaymentMethodAsync(PaymentMethodRequest request);
        Task<PaymentMethod> GetPaymentMethodAsync(string paymentMethodToken);
    }
}
