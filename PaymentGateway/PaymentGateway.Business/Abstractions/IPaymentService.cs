﻿using PaymentGateway.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaymentGateway.Business
{
    public interface IPaymentsService
    {
        Task<Payment> GetAsync(string id);
        Task<Payment> CreateAsync(PaymentRequest request);
    }
}
