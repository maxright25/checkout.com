﻿using PaymentGateway.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Business
{
    public interface IBankProvider
    {
        string BankName { get; }
        Task<BankTransaction> CreateTransactionAsync(BankTransactionRequest request);
    }
}
