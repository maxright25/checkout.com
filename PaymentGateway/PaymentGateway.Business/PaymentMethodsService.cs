﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using PaymentGateway.Entities;
using PaymentGateway.Repository;

namespace PaymentGateway.Business
{
    public class PaymentMethodsService : IPaymentMethodsService
    {
        private readonly IPaymentMethodsRepository _repository;

        public PaymentMethodsService(IPaymentMethodsRepository repository)
        {
            _repository = repository;
        }

        public async Task<PaymentMethod> CreatePaymentMethodAsync(PaymentMethodRequest request)
        {
            var paymentMethod = new PaymentMethod()
            {
                BankName = request.BankName,
                CardholderName = request.CardholderName,
                ExpiryDate = request.ExpiryDate,
                Id = request.Id,
                MaskedNumber = MaskNumber(request.CardNumber),
            };

            await _repository.CreatePaymentMethodAsync(paymentMethod).ConfigureAwait(false);

            return paymentMethod;
        }

        public Task<PaymentMethod> GetPaymentMethodAsync(string paymentMethodToken)
        {
            return _repository.GetPaymentMethodAsync(paymentMethodToken);
        }

        private string MaskNumber(string cardNumber)
        {
            return $"************{cardNumber.Substring(12)}";
        }
    }
}
