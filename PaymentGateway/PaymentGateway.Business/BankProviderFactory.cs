﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Business
{
    public class BankProviderFactory : IBankProviderFactory
    {
        private readonly IComponentContext _context;

        public BankProviderFactory(IComponentContext context)
        {
            _context = context;
        }

        public IBankProvider GetProvider(string cardNumber) => ResolveSandbox();
        public IBankProvider GetProviderByBankName(string bankName) => ResolveSandbox();
        private IBankProvider ResolveSandbox() => _context.ResolveNamed<IBankProvider>("sandbox");
    }
}
