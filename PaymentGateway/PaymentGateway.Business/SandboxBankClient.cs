﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using PaymentGateway.Entities;

namespace PaymentGateway.Business
{
    public class SandboxBankClient : IBankProvider
    {
        private static readonly Regex _expiryDateFormatRegex = new Regex(@"\d{2}/\d{2}", RegexOptions.Compiled);
        private static readonly Regex _cardNumberFormat = new Regex(@"\d{16}", RegexOptions.Compiled);
        private static readonly Regex _cvvNumberFormat = new Regex(@"\d{3}", RegexOptions.Compiled);
        private static readonly Regex _spaceRegex = new Regex(@"\s", RegexOptions.Compiled);

        public string BankName => "Sandbox";

        public async Task<BankTransaction> CreateTransactionAsync(BankTransactionRequest request)
        {
            await Task.Yield();

            var resultTransaction = new BankTransaction()
            {
                Amount = request.Amount,
                CreatedDate = DateTime.UtcNow,
                Id = Guid.NewGuid().ToString(),
                Currency = request.Currency,
                PaymentMethodToken = request.PaymentMethodToken,
                IsSuccess = false
            };

            if (!IsCardDataValid(request))
            {
                resultTransaction.ResponseCode = 3000;
                resultTransaction.ResponseText = "Invalid card";
            } else if (request.Amount <= 0) {
                resultTransaction.ResponseCode = 3001;
                resultTransaction.ResponseText = "Negative amount";
            } else {
                resultTransaction.ResponseCode = 1000;
                resultTransaction.IsSuccess = true;

                // generate new payment token for successful transactions
                if (string.IsNullOrEmpty(resultTransaction.PaymentMethodToken))
                    resultTransaction.PaymentMethodToken = Guid.NewGuid().ToString();
            }

            return resultTransaction;
        }

        // naive card validation. must be much stronger in the real world.
        private bool IsCardDataValid(BankTransactionRequest request)
        {
            var trimmedNumber = _spaceRegex.Replace(request.CardNumber, string.Empty);

            if (!_cardNumberFormat.IsMatch(trimmedNumber))
                return false;

            if (!_cvvNumberFormat.IsMatch(request.CVV))
                return false;

            if (!_expiryDateFormatRegex.IsMatch(request.ExpiryDate))
                return false;

            if (string.IsNullOrEmpty(request.CardholderName))
                return false;
            
            return true;
        }
    }
}
