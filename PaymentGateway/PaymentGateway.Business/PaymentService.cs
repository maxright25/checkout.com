﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PaymentGateway.Entities;
using PaymentGateway.Repository;

namespace PaymentGateway.Business
{
    public class PaymentService : IPaymentsService
    {
        private readonly IPaymentsRepository _paymentsRepository;
        private readonly IPaymentMethodsService _paymentMethodsService;
        private readonly IBankProviderFactory _bankFactory;

        public PaymentService(IPaymentsRepository paymentsRepository, IPaymentMethodsService paymentMethodsService, IBankProviderFactory bankFactory)
        {
            _paymentsRepository = paymentsRepository;
            _paymentMethodsService = paymentMethodsService;
            _bankFactory = bankFactory;
        }

        public Task<Payment> CreateAsync(PaymentRequest request)
        {
            var bankRequest = new BankTransactionRequest()
            {
                Amount = request.Amount.Value,
                CardholderName = request.CardholderName,
                CardNumber = request.CardNumber,
                Currency = request.Amount.Currency,
                CVV = request.CVV,
                ExpiryDate = request.ExpiryDate,
                PaymentMethodToken = request.PaymentMethodToken
            };

            return string.IsNullOrEmpty(request.PaymentMethodToken)
                ? ProcessPaymentUsingNewCard(bankRequest)
                : ProcessPaymentUsingToken(bankRequest);

        }

        public Task<Payment> GetAsync(string id)
        {
            return _paymentsRepository.GetPaymentAsync(id);
        }

        private async Task<Payment> ProcessPaymentUsingNewCard(BankTransactionRequest request)
        {
            var bankClient = _bankFactory.GetProvider(request.CardNumber);
            var transaction = await bankClient.CreateTransactionAsync(request);

            if (transaction.IsSuccess && !string.IsNullOrEmpty(transaction.PaymentMethodToken))
            {
                // save the payment method for future reuse
                var paymentMethodRequest = new PaymentMethodRequest()
                {
                    Id = transaction.PaymentMethodToken,
                    ExpiryDate = request.ExpiryDate,
                    CardNumber = request.CardNumber,
                    CardholderName = request.CardholderName,
                    BankName = bankClient.BankName,
                };

                var paymentMethod = await _paymentMethodsService.CreatePaymentMethodAsync(paymentMethodRequest).ConfigureAwait(false);

                return await SaveBankTransactionAsync(transaction, paymentMethod).ConfigureAwait(false);
            } else {
                return await SaveBankTransactionAsync(transaction, null).ConfigureAwait(false);
            }
        }

        private async Task<Payment> ProcessPaymentUsingToken(BankTransactionRequest request)
        {
            var paymentMethod = await _paymentMethodsService.GetPaymentMethodAsync(request.PaymentMethodToken).ConfigureAwait(false);
            var bankClient = _bankFactory.GetProviderByBankName(paymentMethod.BankName);
            var transaction = await bankClient.CreateTransactionAsync(request);
            return await SaveBankTransactionAsync(transaction, paymentMethod);
        }

        private async Task<Payment> SaveBankTransactionAsync(BankTransaction transaction, PaymentMethod paymentMethod)
        {
            var payment = new Payment()
            {
                Id = transaction.Id,
                Amount = new Money(transaction.Amount, transaction.Currency),
                CreatedDate = transaction.CreatedDate,
                IsSuccess = transaction.IsSuccess,
                ResponseCode = transaction.ResponseCode,
                ResponseText = transaction.ResponseText,
                PaymentMethod = paymentMethod
            };

            await _paymentsRepository.CreatePaymentAsync(payment).ConfigureAwait(false);

            return payment;
        }
    }
}
