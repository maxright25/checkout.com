﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Entities
{
    public class PaymentRequest
    {
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CardholderName { get; set; }
        public string CVV { get; set; }
        // if we pay with the same card twice, we don't need all details again
        public string PaymentMethodToken { get; set; }
        public Money Amount { get; set; }
    }
}
