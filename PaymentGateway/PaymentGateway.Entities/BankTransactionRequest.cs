﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Entities
{
    /// <summary>
    /// Entity for simulation of external bank transaction requests
    /// </summary>
    public class BankTransactionRequest
    {
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CardholderName { get; set; }
        public string CVV { get; set; }
        public string PaymentMethodToken { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }

    }
}
