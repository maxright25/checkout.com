﻿using System;

namespace PaymentGateway.Entities
{
    public class PaymentMethod
    {
        public string Id { get; set; }
        public string MaskedNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CardholderName { get; set; }
        public string BankName { get; set; }
    }
}
