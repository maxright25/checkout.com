﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Entities
{
    /// <summary>
    /// Entity for simulation of external bank transactions
    /// </summary>
    public class BankTransaction
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsSuccess { get; set; }
        public int ResponseCode { get; set; }
        public string ResponseText { get; set; }
        public string PaymentMethodToken { get; set; }
    }
}
