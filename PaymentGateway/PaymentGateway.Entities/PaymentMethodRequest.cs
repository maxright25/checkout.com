﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Entities
{
    public class PaymentMethodRequest
    {
        public string Id { get; set; }
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CardholderName { get; set; }
        public string BankName { get; set; }
    }
}
