using Autofac;
using NUnit.Framework;
using PaymentGateway.Business;
using PaymentGateway.Controllers;
using PaymentGateway.Entities;
using System.Threading.Tasks;

namespace PaymentGateway.API.Tests
{
    public class PaymentsControllerTests
    {
        private IContainer container;

        [OneTimeSetUp]
        public void Setup()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new AutofacBusinessModule());
            builder.RegisterModule(new AutofacRepositoryModule());

            container = builder.Build();
        }

        private PaymentsController GetSut()
        {
            return new PaymentsController(container.Resolve<IPaymentsService>());
        }

        [Test]
        public async Task CreateAsync_WhenPaymentWithNewCardIsSuccessful_ReturnPaymentWithPaymentMethod()
        {
            // Arrange
            var sut = GetSut();
            var paymentRequest = GetValidRequest();
            
            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            Assert.IsNotNull(payment);
            Assert.IsTrue(payment.IsSuccess);
            Assert.IsNotNull(payment.PaymentMethod);
        }

        [Test]
        public async Task CreateAsync_WhenPaymentWithPaymentTokenIsSuccessful_ShouldReturnPaymentWithOldPaymentMethod()
        {
            // Arrange
            var sut = GetSut();
            var paymentRequest = GetValidRequest();
            var firstPayment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);
            paymentRequest.PaymentMethodToken = firstPayment.PaymentMethod.Id;
            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            Assert.IsNotNull(payment);
            Assert.IsTrue(payment.IsSuccess);
            Assert.AreEqual(1000, payment.ResponseCode);
            Assert.AreEqual(payment.PaymentMethod.Id, firstPayment.PaymentMethod.Id);
        }

        [Test]
        public async Task CreateAsync_WhenCardInfoIsInvalid_ShouldReturnFailedPayment()
        {
            // Arrange
            var sut = GetSut();
            var paymentRequest = GetValidRequest();
            paymentRequest.CVV = "Fuuuuu";

            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            Assert.IsNotNull(payment);
            Assert.IsFalse(payment.IsSuccess);
            Assert.AreEqual(3000, payment.ResponseCode);
            Assert.IsNotNull(payment.ResponseText);
        }

        [Test]
        public async Task CreateAsync_WhenAmountIsLessThanZero_ShouldReturnFailedPayment()
        {
            // Arrange
            var sut = GetSut();
            var paymentRequest = GetValidRequest();
            paymentRequest.Amount = new Money(-100, "USD");

            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            Assert.IsNotNull(payment);
            Assert.IsFalse(payment.IsSuccess);
            Assert.AreEqual(3001, payment.ResponseCode);
            Assert.IsNotNull(payment.ResponseText);
        }

        [Test]
        public async Task GetAsync_WhenExistingPaymentIdPassed_ReturnPayment()
        {
            // Arrange
            var sut = GetSut();
            var paymentRequest = GetValidRequest();
            var expectedPayment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Act
            var actualPayment = await sut.GetAsync(expectedPayment.Id).ConfigureAwait(false);

            // Assert
            Assert.IsNotNull(actualPayment);
            Assert.AreEqual(actualPayment.Id, expectedPayment.Id);
            Assert.AreEqual(actualPayment.CreatedDate, expectedPayment.CreatedDate);
            Assert.AreEqual(actualPayment.IsSuccess, expectedPayment.IsSuccess);
            Assert.AreEqual(actualPayment.PaymentMethod.Id, expectedPayment.PaymentMethod.Id);
        }

        [Test]
        public async Task GetAsync_WhenNonExistingPaymentIdPassed_ReturnNull()
        {
            // Arrange
            var sut = GetSut();

            // Act
            var actualPayment = await sut.GetAsync("id").ConfigureAwait(false);

            // Assert
            Assert.IsNull(actualPayment);
        }

        private PaymentRequest GetValidRequest()
        {
            return new PaymentRequest
            {
                Amount = new Money(100, "USD"),
                CardholderName = "John Doe",
                CardNumber = "1234123412341234",
                CVV = "012",
                ExpiryDate = "10/20",
            };
        }
    }
}