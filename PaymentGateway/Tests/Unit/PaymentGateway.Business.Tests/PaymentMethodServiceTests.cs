using AutoFixture;
using Moq;
using NUnit.Framework;
using PaymentGateway.Entities;
using PaymentGateway.Repository;
using System.Threading.Tasks;

namespace PaymentGateway.Business.Tests
{
    public class PaymentMethodsServiceTests
    {
        private Mock<IPaymentMethodsRepository> _paymentMethodsRepository;

        [SetUp]
        public void Init()
        {
            _paymentMethodsRepository = new Mock<IPaymentMethodsRepository>();
        }

        private IPaymentMethodsService GetSut()
        {
            return new PaymentMethodsService(_paymentMethodsRepository.Object);
        }

        [Test]
        public async Task CreatePaymentMethodAsync_WhenRequestIsWellFormed_ShouldSavePaymentMethodAsync()
        {
            // Arrange
            var pmRequest = GetValidRequest();
            var sut = GetSut();

            // Act
            await sut.CreatePaymentMethodAsync(pmRequest).ConfigureAwait(false);

            // Assert
            _paymentMethodsRepository.Verify(x => x.CreatePaymentMethodAsync(It.IsAny<PaymentMethod>()), Times.Once);
        }

        [Test]
        public async Task CreatePaymentMethodAsync_WhenRequestIsWellFormed_ShouldReturnMaskedCard()
        {
            // Arrange
            var pmRequest = GetValidRequest();
            var sut = GetSut();

            // Act
            var paymentMethod = await sut.CreatePaymentMethodAsync(pmRequest).ConfigureAwait(false);

            // Assert
            Assert.IsTrue(paymentMethod.MaskedNumber.StartsWith("************"));
        }

        private PaymentMethodRequest GetValidRequest()
        {
            return new PaymentMethodRequest
            {
                CardNumber = "1234123412341234",
                CardholderName = "John Doe",
                ExpiryDate = "10/20",
                Id = "abcdef",
                BankName = "HSBC"
            };
        }
    }
}