using AutoFixture;
using Moq;
using NUnit.Framework;
using PaymentGateway.Entities;
using PaymentGateway.Repository;

namespace PaymentGateway.Business.Tests
{
    public class PaymentServiceTests
    {
        private Mock<IPaymentsRepository> _paymentRepository;
        private Mock<IPaymentMethodsService> _paymentMethodsService;
        private Mock<IBankProvider> _bankProvider;
        private Mock<IBankProviderFactory> _bankProviderFactory;
        private IFixture _fixture = new Fixture();

        [SetUp]
        public void Init()
        {
            _paymentMethodsService = new Mock<IPaymentMethodsService>();
            _paymentRepository = new Mock<IPaymentsRepository>();
            _bankProvider = new Mock<IBankProvider>();
            _bankProviderFactory = new Mock<IBankProviderFactory>();

            // configure default setups
            _paymentMethodsService.Setup(x => x.CreatePaymentMethodAsync(It.IsAny<PaymentMethodRequest>())).ReturnsAsync(_fixture.Create<PaymentMethod>());
            _bankProvider.Setup(x => x.CreateTransactionAsync(It.IsAny<BankTransactionRequest>())).ReturnsAsync(_fixture.Create<BankTransaction>());
            _bankProviderFactory.Setup(x => x.GetProvider(It.IsAny<string>())).Returns(_bankProvider.Object);
            _bankProviderFactory.Setup(x => x.GetProviderByBankName(It.IsAny<string>())).Returns(_bankProvider.Object);
        }

        private IPaymentsService GetSut()
        {
            return new PaymentService(_paymentRepository.Object, _paymentMethodsService.Object, _bankProviderFactory.Object);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_WhenCardIsNew_ShouldResolveProviderUsingCardNumber()
        {
            // Arrange
            var expectedCardNumber =  _fixture.Create<string>();
            var request = _fixture.Create<PaymentRequest>();

            request.CardNumber = expectedCardNumber;
            request.PaymentMethodToken = string.Empty;

            var sut = GetSut();

            // Act
            var payment = await sut.CreateAsync(request).ConfigureAwait(false);

            // Assert
            _bankProviderFactory.Verify(x => x.GetProvider(expectedCardNumber), Times.Once);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_WhenCardIsNew_ShouldReturnPayment()
        {
            // Arrange
            var paymentRequest = _fixture.Create<PaymentRequest>();
            paymentRequest.PaymentMethodToken = string.Empty;

            var bankResponse = _fixture.Create<BankTransaction>();

            _bankProvider
                .Setup(x => x.CreateTransactionAsync(It.IsAny<BankTransactionRequest>()))
                .ReturnsAsync(bankResponse);

            var sut = GetSut();

            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            Assert.AreEqual(payment.ResponseCode, bankResponse.ResponseCode);
            Assert.AreEqual(payment.ResponseText, bankResponse.ResponseText);
            Assert.AreEqual(payment.CreatedDate, bankResponse.CreatedDate);
            Assert.AreEqual(payment.Amount.Currency, bankResponse.Currency);
            Assert.AreEqual(payment.Amount.Value, bankResponse.Amount);
            Assert.AreEqual(payment.IsSuccess, bankResponse.IsSuccess);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_WhenCardIsNew_ShouldSavePayment()
        {
            // Arrange
            var expectedTransactionId = _fixture.Create<string>();
            var paymentRequest = _fixture.Create<PaymentRequest>();
            paymentRequest.PaymentMethodToken = string.Empty;

            var bankResponse = _fixture.Create<BankTransaction>();
            bankResponse.Id = expectedTransactionId;

            _bankProvider
                .Setup(x => x.CreateTransactionAsync(It.IsAny<BankTransactionRequest>()))
                .ReturnsAsync(bankResponse);

            var sut = GetSut();

            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            _paymentRepository.Verify(x => x.CreatePaymentAsync(It.Is<Payment>(pmnt => pmnt.Id == expectedTransactionId)), Times.Once);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_WhenCardIsNewAndTransactionIsSuccessful_ShouldSavePaymentMethod()
        {
            // Arrange
            var expectedCardNumber =  _fixture.Create<string>();
            var expectedToken =  _fixture.Create<string>();

            var paymentRequest = _fixture.Create<PaymentRequest>();
            paymentRequest.CardNumber = expectedCardNumber;
            paymentRequest.PaymentMethodToken = string.Empty;

            var bankResponse = _fixture.Create<BankTransaction>();
            bankResponse.IsSuccess = true;
            bankResponse.PaymentMethodToken = expectedToken;

            _bankProvider
                .Setup(x => x.CreateTransactionAsync(It.Is<BankTransactionRequest>(y => y.CardNumber == expectedCardNumber)))
                .ReturnsAsync(bankResponse);

            var sut = GetSut();

            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            _paymentMethodsService.Verify(x => x.CreatePaymentMethodAsync(It.Is<PaymentMethodRequest>(y => y.Id == expectedToken)), Times.Once);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_WhenTokenIsPresented_ShouldRetrievePaymentMethod()
        {
            // Arrange
            var expectedToken =  _fixture.Create<string>();

            var paymentRequest = _fixture.Create<PaymentRequest>();
            paymentRequest.PaymentMethodToken = expectedToken;

            var paymentMethod = _fixture.Create<PaymentMethod>();

            _paymentMethodsService
                .Setup(x => x.GetPaymentMethodAsync(expectedToken))
                .ReturnsAsync(paymentMethod);

            var sut = GetSut();

            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            _paymentMethodsService.Verify(x => x.GetPaymentMethodAsync(expectedToken), Times.Once);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_WhenTokenIsPresented_ShouldResolveProviderUsingBankName()
        {
            // Arrange
            var expectedToken =  _fixture.Create<string>();
            var expectedBankName =  _fixture.Create<string>();

            var paymentRequest = _fixture.Create<PaymentRequest>();
            paymentRequest.PaymentMethodToken = expectedToken;

            var paymentMethod = _fixture.Create<PaymentMethod>();
            paymentMethod.BankName = expectedBankName;

            _paymentMethodsService
                .Setup(x => x.GetPaymentMethodAsync(expectedToken))
                .ReturnsAsync(paymentMethod);

            var sut = GetSut();

            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            _bankProviderFactory.Verify(x => x.GetProviderByBankName(expectedBankName), Times.Once);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateAsync_WhenTokenIsPresented_ShouldReturnPayment()
        {
            // Arrange
            var paymentRequest = _fixture.Create<PaymentRequest>();
            var paymentMethod = _fixture.Create<PaymentMethod>();
            var bankResponse = _fixture.Create<BankTransaction>();

            _paymentMethodsService
                .Setup(x => x.GetPaymentMethodAsync(It.IsAny<string>()))
                .ReturnsAsync(paymentMethod);

            _bankProvider
                .Setup(x => x.CreateTransactionAsync(It.IsAny<BankTransactionRequest>()))
                .ReturnsAsync(bankResponse);

            var sut = GetSut();

            // Act
            var payment = await sut.CreateAsync(paymentRequest).ConfigureAwait(false);

            // Assert
            Assert.AreEqual(payment.ResponseCode, bankResponse.ResponseCode);
            Assert.AreEqual(payment.ResponseText, bankResponse.ResponseText);
            Assert.AreEqual(payment.CreatedDate, bankResponse.CreatedDate);
            Assert.AreEqual(payment.Amount.Currency, bankResponse.Currency);
            Assert.AreEqual(payment.Amount.Value, bankResponse.Amount);
            Assert.AreEqual(payment.IsSuccess, bankResponse.IsSuccess);
            Assert.AreEqual(payment.PaymentMethod, paymentMethod);
        }
    }
}