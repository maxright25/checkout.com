﻿using Autofac;
using PaymentGateway.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGateway.Business
{
    public class AutofacRepositoryModule : Module
    {
        public AutofacRepositoryModule()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<InMemoryPaymentRepository>()
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterType<InMemoryPaymentMethodsRepository>()
                .AsImplementedInterfaces()
                .SingleInstance();
        }
    }
}
