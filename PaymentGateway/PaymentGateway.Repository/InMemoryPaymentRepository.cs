﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaymentGateway.Entities;

namespace PaymentGateway.Repository
{
    public class InMemoryPaymentRepository : IPaymentsRepository
    {
        private readonly Dictionary<string, Payment> _payments = new Dictionary<string, Payment>();
        private readonly IPaymentMethodsRepository _paymentMethodsRepository;

        public InMemoryPaymentRepository(IPaymentMethodsRepository paymentMethodsRepository)
        {
            _paymentMethodsRepository = paymentMethodsRepository;
        }

        public async Task CreatePaymentAsync(Payment payment)
        {
            await Task.Yield();
            _payments[payment.Id] = payment;
        }

        public async Task<Payment> GetPaymentAsync(string paymentId)
        {
            if (!_payments.ContainsKey(paymentId))
                return null;

            var payment = _payments[paymentId];
            if (!string.IsNullOrEmpty(payment.PaymentMethod?.Id))
            {
                payment.PaymentMethod = await _paymentMethodsRepository.GetPaymentMethodAsync(payment.PaymentMethod.Id).ConfigureAwait(false);
            }
            return payment;
        }
    }
}
