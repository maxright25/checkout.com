﻿using PaymentGateway.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PaymentGateway.Repository
{
    public interface IPaymentMethodsRepository
    {
        Task<PaymentMethod> GetPaymentMethodAsync(string paymentMethodToken);
        Task CreatePaymentMethodAsync(PaymentMethod paymentMethod);
    }
}
