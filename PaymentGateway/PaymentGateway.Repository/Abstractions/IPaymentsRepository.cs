﻿using PaymentGateway.Entities;
using System;
using System.Threading.Tasks;

namespace PaymentGateway.Repository
{
    public interface IPaymentsRepository
    {
        Task CreatePaymentAsync(Payment payment);
        Task<Payment> GetPaymentAsync(string paymentId);
    }
}
