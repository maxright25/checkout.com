﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PaymentGateway.Entities;

namespace PaymentGateway.Repository
{
    public class InMemoryPaymentMethodsRepository : IPaymentMethodsRepository
    {
        private readonly Dictionary<string, PaymentMethod> _paymentMethods = new Dictionary<string, PaymentMethod>();

        public async Task CreatePaymentMethodAsync(PaymentMethod paymentMethod)
        {
            await Task.Yield();
            _paymentMethods[paymentMethod.Id] = paymentMethod;
        }

        public async Task<PaymentMethod> GetPaymentMethodAsync(string paymentMethodToken)
        {
            await Task.Yield();
            return _paymentMethods[paymentMethodToken];
        }
    }
}
